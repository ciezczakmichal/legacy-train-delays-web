import React from 'react'
import './Timeline.scss'

import { RefreshButton } from '../../components/Buttons'
import TimelineItem from './TimelineItem'

const Timeline = props => {
    let prevItem = null
    const stations = props.data.map((item, index) => {
        const result = (
            <TimelineItem
                // nie używaj ID stacji, może się bowiem powtórzyć na trasie pociągu
                key={index}
                data={item}
                prevData={prevItem}
                trainStartedRoute={props.trainStartedRoute}
                store={props.store}
            />
        )
        prevItem = item
        return result
    })

    return (
        <div className={'timeline-container ' + props.className}>
            <div className="timeline-header">
                <h2>Trasa pociągu stacja po stacji</h2>
                <RefreshButton onClick={props.onRefreshClick} />
            </div>
            <div className="fetch-time">
                Dane pobrano: {props.fetchTimeString}
            </div>
            <ul className="timeline">{stations}</ul>
        </div>
    )
}

export default Timeline
