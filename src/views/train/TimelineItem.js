import React from 'react'
import './TimelineItem.scss'

const ScheduleItem = props => {
    const isDelayed = props.delay > 0
    const classIfDelayed = className => (isDelayed ? ` ${className}` : '')

    return (
        <div className={props.className + classIfDelayed('delayed')}>
            <span className="label">{props.label}</span>
            <span className={'time' + classIfDelayed('strikethrough')}>
                {props.time}
            </span>
            {isDelayed && (
                <>
                    <span className="current-time">{props.currentTime}</span>
                    <span className="delay">(+{props.delay} min)</span>
                </>
            )}
        </div>
    )
}

const getDelay = data => {
    return data.arrivalTime !== null ? data.arrivalDelay : data.departureDelay
}

const getDelayClassName = delay => {
    if (delay < 6) {
        return 'time-no-delay'
    } else if (delay < 16) {
        return 'time-slight-delay'
    } else {
        return 'time-delay'
    }
}

const TimelineItem = props => {
    const data = props.data
    const station = props.store.getStationById(data.stationId)

    let classNames = ''

    if (props.trainStartedRoute) {
        classNames = getDelayClassName(getDelay(data))

        if (props.prevData !== null) {
            classNames += ' prev-' + getDelayClassName(getDelay(props.prevData))
        }
    }

    classNames += data.alreadyArrived ? ' train-arrived' : ' train-not-arrived'

    return (
        <li className={'timeline-item ' + classNames}>
            <div className="icon-container">
                <div className="icon"></div>
            </div>
            <div className="station-data">
                <p className="name">{station.name}</p>
                {data.arrivalTime !== null && (
                    <ScheduleItem
                        className="arrival"
                        label="Przyjazd"
                        time={data.arrivalTime}
                        currentTime={data.currentArrivalTime}
                        delay={data.arrivalDelay}
                    />
                )}
                {data.departureTime !== null && (
                    <ScheduleItem
                        className="departure"
                        label="Odjazd"
                        time={data.departureTime}
                        currentTime={data.currentDepartureTime}
                        delay={data.departureDelay}
                    />
                )}
            </div>
        </li>
    )
}

export default TimelineItem
