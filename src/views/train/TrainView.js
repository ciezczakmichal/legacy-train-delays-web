import React from 'react'
import { withRouter } from 'react-router-dom'
import './TrainView.scss'

import Timeline from './Timeline'
import CarrierLogo from '../../components/CarrierLogo'
import Loader from '../../components/Loader'
import API from '../../helpers/API'
import {
    faStar as faStarSolid,
    faArrowLeft,
} from '@fortawesome/free-solid-svg-icons'
import { faStar as faStarRegular } from '@fortawesome/free-regular-svg-icons'
import { LineButton } from '../../components/Buttons'
import { ButtonLink } from '../../components/ButtonLink'

class TrainView extends React.Component {
    state = {
        isLoaded: false,
        data: null,
        fetchTimeString: null,
        errorMessage: null,
    }

    canReturnToStationView() {
        const state = this.props.location.state
        return state && typeof state.callerStationId === 'number'
    }

    getTrainId() {
        return this.props.match.params.id
    }

    handleReturnToStationView = () => {
        if (this.canReturnToStationView()) {
            this.props.history.goBack()
        }
    }

    handleRefreshData = () => {
        this.setState({
            isLoaded: false,
            data: null,
            fetchTimeString: null,
            errorMessage: null,
        })

        this.fetchTrainData()
    }

    fetchTrainData() {
        API.fetchTrain(this.getTrainId())
            .then(data => {
                this.setState({
                    isLoaded: true,
                    data,
                    fetchTimeString: new Date().toLocaleString(),
                })
            })
            .catch(error => {
                console.error(error)
                this.setState({
                    errorMessage: error.message,
                })
            })
    }

    componentDidMount() {
        this.fetchTrainData()
    }

    render() {
        const store = this.props.store
        const { isLoaded, errorMessage, data, fetchTimeString } = this.state
        const id = this.getTrainId()

        const trackedButton = store.isTrainTracked(id) ? (
            <LineButton
                className="track-button"
                icon={faStarSolid}
                onClick={() => store.deleteTrackedTrain(id)}
            >
                Usuń z obserwowanych
            </LineButton>
        ) : (
            <LineButton
                className="track-button"
                icon={faStarRegular}
                onClick={() => store.addTrackedTrain(id)}
            >
                Dodaj do obserwowanych
            </LineButton>
        )

        const returnButton = this.canReturnToStationView() ? (
            <LineButton
                className="return-button"
                icon={faArrowLeft}
                color="secondary"
                onClick={this.handleReturnToStationView}
            >
                Wróć na widok stacji
            </LineButton>
        ) : (
            <ButtonLink
                className="return-button"
                icon={faArrowLeft}
                color="secondary"
                to="/"
            >
                Wróć na ekran główny
            </ButtonLink>
        )

        return (
            <div className="train-view">
                {!isLoaded || errorMessage !== null ? (
                    <Loader
                        title="Trwa pobieranie danych..."
                        errorTitle="Pobranie danych pociągu nie powiodło się"
                        errorMessage={errorMessage}
                    />
                ) : (
                    <>
                        <div className="train-info-container">
                            <div className="train-info">
                                <div className="info-block">
                                    <div className="carrier-and-number">
                                        <div className="carrier-logo">
                                            <CarrierLogo
                                                carrierId={data.carrierId}
                                                store={store}
                                            />
                                        </div>
                                        <div className="train-number">
                                            {data.trainNumber}
                                        </div>
                                    </div>
                                    {data.trainName !== null && (
                                        <div className="train-name">
                                            {data.trainName}
                                        </div>
                                    )}
                                </div>
                                <div className="route-block">
                                    <div className="title">Pociąg relacji</div>
                                    <div className="route">
                                        {data.routeString}
                                    </div>
                                </div>
                                <div className="actions">
                                    {trackedButton}
                                    {returnButton}
                                </div>
                            </div>
                        </div>
                        <Timeline
                            className="train-timeline"
                            data={data.timeline}
                            trainStartedRoute={data.hasStartedRoute}
                            fetchTimeString={fetchTimeString}
                            store={store}
                            onRefreshClick={this.handleRefreshData}
                        />
                    </>
                )}
            </div>
        )
    }
}

export default withRouter(TrainView)
