import React from 'react'
import './Train.scss'

import CarrierLogo from '../../components/CarrierLogo'

const Train = props => {
    const store = props.store

    const isDelayed = props.delay > 0
    const classIfDelayed = className => (isDelayed ? ` ${className}` : '')

    return (
        <li className="train" onClick={() => props.onClick(props.id)}>
            <div className="timebox-container">
                <div className={'timebox' + classIfDelayed('delayed')}>
                    <div className={'time' + classIfDelayed('strikethrough')}>
                        {props.time}
                    </div>
                    {isDelayed && (
                        <>
                            <div className="current-time">
                                {props.currentTime}
                            </div>
                            <div className="delay">+{props.delay} min</div>
                        </>
                    )}
                </div>
            </div>
            <div className="carrier-logo">
                <CarrierLogo
                    className="logo"
                    carrierId={props.carrierId}
                    store={store}
                />
            </div>
            <div className="info">
                <div className="route">{props.route}</div>
                <div className="train-ids">
                    <span className="train-number">{props.number}</span>
                    <span className="train-name">{props.name}</span>
                </div>
            </div>
        </li>
    )
}

export default Train
