import React from 'react'
import { withRouter } from 'react-router-dom'
import { faArrowLeft, faCaretDown } from '@fortawesome/free-solid-svg-icons'
import './StationView.scss'

import Loader from '../../components/Loader'
import SearchBox from '../../components/SearchBox'
import { RefreshButton, Button } from '../../components/Buttons'
import ViewGroup from './ViewGroup'
import API from '../../helpers/API'
import { goToTrainView } from '../../helpers/routing'
import { ButtonLink } from '../../components/ButtonLink'

const pageItemCount = 8

const parseResponse = response => {
    const trains = response.trains

    const arrivals = trains.filter(
        /* API zwróci dane dane pociągu, który jeszcze nie wyjechał,
         * stąd konieczność filtrowania "alreadyArrived" */
        train =>
            train.dataForStation.arrivalTime !== null &&
            !train.dataForStation.alreadyArrived
    )
    const departures = trains
        .filter(train => train.dataForStation.departureTime !== null)
        .sort(
            (a, b) =>
                a.dataForStation.currentDepartureTime >
                b.dataForStation.currentDepartureTime
        )

    return { arrivals, departures }
}

const filterTrains = (trains, page) => {
    const count = page * pageItemCount
    const arrivals = trains.arrivals.slice(0, count)
    const departures = trains.departures.slice(0, count)

    return { arrivals, departures }
}

const endPageForTrains = trains => {
    const length = Math.max(trains.arrivals.length, trains.departures.length)
    return Math.ceil(length / pageItemCount)
}

class StationView extends React.Component {
    state = {
        // komponent nowo stworzony - jeśli nie pobieramy danych, ale ich też nie mamy
        justConstructed: true,

        fetchingDataForStationId: null,
        trainsForStationId: null,
        trains: null,
        fetchTimeString: null,
        errorMessage: null,

        visibleTrains: null,
        page: 0,
        endPage: 0,
    }

    isFetchingData() {
        return this.state.fetchingDataForStationId !== null
    }

    getStationData() {
        const urlName = this.props.match.params.urlName
        return this.props.store.getStationByUrlName(urlName)
    }

    updateLastSelectedStations() {
        const store = this.props.store
        const station = this.getStationData()

        if (store.lastSelectedStationId() !== station.id) {
            store.addLastSelectedStation(station.id)
        }
    }

    incrementPage = () => {
        const nextPage = this.state.page + 1

        this.setState({
            visibleTrains: filterTrains(this.state.trains, nextPage),
            page: nextPage,
        })
    }

    fetchStationData() {
        const station = this.getStationData()

        this.setState({
            fetchingDataForStationId: station.id,
            fetchTimeString: null,
        })

        const dataForCurrentStation = state =>
            state.fetchingDataForStationId === station.id

        API.fetchStationTrains(station.id)
            .then(data => {
                const trains = parseResponse(data)
                this.setState(prevState => {
                    return !dataForCurrentStation(prevState)
                        ? {}
                        : {
                              trainsForStationId: station.id,
                              trains,
                              fetchTimeString: new Date().toLocaleString(),
                              page: 0,
                              endPage: endPageForTrains(trains),
                          }
                })
                this.incrementPage()
            })
            .catch(error => {
                console.error(error)
                this.setState({
                    errorMessage: error.message,
                })
            })
            .finally(() => {
                this.setState(prevState => {
                    return !dataForCurrentStation(prevState)
                        ? {}
                        : {
                              fetchingDataForStationId: null,
                          }
                })
            })
    }

    fetchStationDataIfNecessary() {
        const station = this.getStationData()

        if (
            station === null ||
            station.id === this.state.trainsForStationId ||
            station.id === this.state.fetchingDataForStationId ||
            this.state.errorMessage !== null
        ) {
            return
        }

        this.fetchStationData()
    }

    handleRefreshData = () => {
        this.fetchStationData()
    }

    handleTrainClick = trainId => {
        const station = this.getStationData()
        goToTrainView(this.props.history, trainId, station.id)
    }

    componentDidMount() {
        this.setState({
            justConstructed: false,
        })

        this.updateLastSelectedStations()
        this.fetchStationData()
    }

    componentDidUpdate() {
        this.updateLastSelectedStations()
        this.fetchStationDataIfNecessary()
    }

    render() {
        const store = this.props.store
        const station = this.getStationData()
        const {
            justConstructed,
            visibleTrains,
            fetchTimeString,
            errorMessage,
            page,
            endPage,
        } = this.state

        const noData =
            visibleTrains !== null &&
            visibleTrains.arrivals.length === 0 &&
            visibleTrains.departures.length === 0

        const noMoreData = page >= endPage

        return (
            <div className="station-view">
                <SearchBox defaultStationById={station.id} store={store} />
                {justConstructed ||
                this.isFetchingData() ||
                errorMessage !== null ? (
                    <Loader
                        title="Trwa pobieranie danych..."
                        errorTitle="Pobranie danych dla stacji nie powiodło się"
                        errorMessage={errorMessage}
                    />
                ) : (
                    <div className="station-data">
                        <div className="station-header">
                            <h2>
                                Pociągi na stacji{' '}
                                <span className="station-name">
                                    {station.name}
                                </span>
                            </h2>
                            <RefreshButton
                                className="refresh-btn"
                                onClick={this.handleRefreshData}
                            />
                        </div>
                        <div className="fetch-time">
                            Dane pobrano: {fetchTimeString}
                        </div>
                        {noData ? (
                            <p className="no-data">
                                Brak obecnie połączeń kolejowych dla tej stacji.
                            </p>
                        ) : (
                            <div className="views">
                                <ViewGroup
                                    itemsClassName="items-border"
                                    title="Przyjazdy"
                                    timeProperty="arrival"
                                    trains={visibleTrains.arrivals}
                                    store={store}
                                    onTrainClick={this.handleTrainClick}
                                />
                                <ViewGroup
                                    title="Odjazdy"
                                    timeProperty="departure"
                                    trains={visibleTrains.departures}
                                    store={store}
                                    onTrainClick={this.handleTrainClick}
                                />
                            </div>
                        )}
                        {noMoreData ? null : (
                            <Button
                                className="next-page"
                                icon={faCaretDown}
                                onClick={this.incrementPage}
                            >
                                Pokaż więcej wyników
                            </Button>
                        )}
                        <ButtonLink
                            className="return-button"
                            icon={faArrowLeft}
                            color="secondary"
                            to="/"
                        >
                            Wróć na ekran główny
                        </ButtonLink>
                    </div>
                )}
            </div>
        )
    }
}

export default withRouter(StationView)
