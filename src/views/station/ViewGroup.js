import React from 'react'
import './ViewGroup.scss'

import Train from './Train'

const ViewGroup = props => {
    const arrival = props.timeProperty === 'arrival'

    const timeProperty = arrival ? 'arrivalTime' : 'departureTime'
    const currentTimeProperty = arrival
        ? 'currentArrivalTime'
        : 'currentDepartureTime'
    const delayProperty = arrival ? 'arrivalDelay' : 'departureDelay'

    const trains = props.trains.map(train => {
        const time = train.dataForStation[timeProperty]
        const currentTime = train.dataForStation[currentTimeProperty]
        const delay = train.dataForStation[delayProperty]

        return (
            <Train
                key={train.id}
                id={train.id}
                carrierId={train.carrierId}
                number={train.trainNumber}
                name={train.trainName}
                route={train.routeString}
                time={time}
                currentTime={currentTime}
                delay={delay}
                store={props.store}
                onClick={props.onTrainClick}
            />
        )
    })

    return (
        <div className="view-group">
            <h3>{props.title}</h3>
            {trains.length === 0 ? (
                <p className="no-items">
                    Brak obecnie połączeń w tej kategorii.
                </p>
            ) : (
                <ul className={['items', props.itemsClassName].join(' ')}>
                    {trains}
                </ul>
            )}
        </div>
    )
}

export default ViewGroup
