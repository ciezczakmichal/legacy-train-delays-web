import React from 'react'
import { withRouter } from 'react-router-dom'
import { goToStationView } from '../../helpers/routing'
import './LastSelected.scss'
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Item = props => {
    const station = props.station
    return (
        <li className="item">
            <div
                className="display-name"
                onClick={() => props.onClick(station)}
            >
                {station.name}
            </div>
            <FontAwesomeIcon
                className="delete-btn"
                icon={faTrashAlt}
                title="Wybierz, aby usunąć z listy"
                onClick={() => props.onDelete(station)}
            />
        </li>
    )
}

const LastSelected = props => {
    const store = props.store

    const handleItemClicked = station => {
        goToStationView(props.history, station.urlName)
    }

    const handleItemDeleted = station => {
        store.deleteLastSelectedStation(station.id)
    }

    const items = store.lastSelected.map(id => {
        const station = store.getStationById(id)

        // nie renderuj niepoprawnych stacji
        if (station === null) {
            return null
        }

        return (
            <Item
                key={station.id}
                station={station}
                onClick={handleItemClicked}
                onDelete={handleItemDeleted}
            />
        )
    })

    const isEmpty = items.length === 0

    return (
        <div className="last-selected">
            <h3 className="component-title">Ostatnio przeglądane stacje</h3>
            <div className="list-container">
                {isEmpty ? (
                    <p>Nie przeglądano jeszcze żadnej stacji</p>
                ) : (
                    <ul className="list">{items}</ul>
                )}
            </div>
        </div>
    )
}

export default withRouter(LastSelected)
