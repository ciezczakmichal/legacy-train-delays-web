import React from 'react'
import './TrackedItem.scss'

import CarrierLogo from '../../components/CarrierLogo'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons'

const TrackedItem = props => {
    const store = props.store

    return (
        <li className="tracked-item">
            <div className="data" onClick={() => props.onClick(props.id)}>
                <div className="carrier-logo">
                    <CarrierLogo
                        className="logo"
                        carrierId={props.carrierId}
                        store={store}
                    />
                </div>
                <div className="info">
                    <div className="route">{props.route}</div>
                    <div className="train-ids">
                        <span className="train-number">{props.number}</span>
                        <span className="train-name">{props.name}</span>
                    </div>
                </div>
            </div>
            <div className="delete-btn-container">
                <FontAwesomeIcon
                    className="delete-btn"
                    icon={faTrashAlt}
                    title="Wybierz, aby usunąć z listy"
                    onClick={() => props.onDelete(props.id)}
                />
            </div>
        </li>
    )
}

export default TrackedItem
