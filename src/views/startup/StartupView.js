import React from 'react'
import './StartupView.scss'

import SearchBox from '../../components/SearchBox'
import LastSelected from './LastSelected'
import TrackedTrains from './TrackedTrains'

const StartupView = props => {
    const store = props.store
    const hasTrackedTrains = store.trackedTrains.length > 0
    const singleClass = hasTrackedTrains ? '' : ' single'

    return (
        <div className="startup-view">
            <SearchBox store={store} />
            <div className="components">
                <div className={'view-item' + singleClass}>
                    <LastSelected store={store} />
                </div>
                {hasTrackedTrains && (
                    <div className="view-item">
                        <TrackedTrains store={store} />
                    </div>
                )}
            </div>
        </div>
    )
}

export default StartupView
