import React from 'react'
import { withRouter, useHistory } from 'react-router-dom'
import './TrackedTrains.scss'

import TrackedItem from './TrackedItem'
import Loader from '../../components/Loader'
import { goToTrainView } from '../../helpers/routing'
import API from '../../helpers/API'

const TrackedTrainsList = props => {
    const store = props.store
    const history = useHistory()

    const handleTrainClick = trainId => {
        goToTrainView(history, trainId, null)
    }

    const handleTrainDelete = trainId => {
        store.deleteTrackedTrain(trainId)
    }

    const trains = store.trackedTrains.map(id => {
        const data = props.trainsData.find(data => data.id === id) || null

        /* ? bieżące opóźnienie, następna planowa stacja + godz. przyjazdu)  */
        return !data ? null : (
            <TrackedItem
                key={id}
                id={id}
                carrierId={data.carrierId}
                route={data.routeString}
                number={data.trainNumber}
                name={data.trainName}
                store={store}
                onClick={handleTrainClick}
                onDelete={handleTrainDelete}
            />
        )
    })

    return <ul className="list">{trains}</ul>
}

class TrackedTrains extends React.Component {
    state = {
        trainsData: [],
        deletedCount: 0,
        errorMessage: null,
    }

    getTrackedTrainsArray() {
        return this.props.store.trackedTrains
    }

    isAllDataReady() {
        return this.getTrackedTrainsArray().every(
            id => this.state.trainsData.findIndex(data => data.id === id) > -1
        )
    }

    fetchTrainsData() {
        this.getTrackedTrainsArray().forEach(id => {
            API.fetchTrain(id)
                .then(data => {
                    // dostępność danych pociągu np. wygasła
                    if (data === null) {
                        this.props.store.deleteTrackedTrain(id)
                        this.setState(prevState => ({
                            deletedCount: prevState.deletedCount + 1,
                        }))
                    } else {
                        this.setState(prevState => ({
                            trainsData: [...prevState.trainsData, data],
                        }))
                    }
                })
                .catch(error => {
                    console.error(error)
                    this.setState({
                        errorMessage: error.message,
                    })
                })
        })
    }

    getDeletedTrainsNotice() {
        const count = this.state.deletedCount
        let texts = []

        if (count > 0) {
            if (count === 1) {
                texts = [
                    `Usunięto ${count} obserwowane połączenie,`,
                    'którego dane nie są już dostępne.',
                ]
            } else if (count < 5) {
                texts = [
                    `Usunięto ${count} obserwowane połączenia,`,
                    'których dane nie są już dostępne.',
                ]
            } else {
                texts = [
                    `Usunięto ${count} obserwowanych połączeń,`,
                    'których dane nie są już dostępne.',
                ]
            }
        }

        return texts.length !== 2 ? null : (
            <p className="deleted-notice">
                {texts[0]}
                <br />
                {texts[1]}
            </p>
        )
    }

    componentDidMount() {
        this.fetchTrainsData()
    }

    render() {
        const store = this.props.store
        const { trainsData, errorMessage } = this.state

        return (
            <div className="tracked-trains">
                <h3 className="component-title">Obserwowane połączenia</h3>
                <div className="list-container">
                    {!this.isAllDataReady() || errorMessage !== null ? (
                        <Loader
                            title="Trwa pobieranie danych..."
                            errorTitle="Pobranie danych połączeń nie powiodło się"
                            errorMessage={errorMessage}
                        />
                    ) : (
                        <>
                            <TrackedTrainsList
                                trainsData={trainsData}
                                store={store}
                            />
                            {this.getDeletedTrainsNotice()}
                        </>
                    )}
                </div>
            </div>
        )
    }
}

export default withRouter(TrackedTrains)
