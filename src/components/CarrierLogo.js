import React from 'react'
import './CarrierLogo.scss'

const CarrierLogo = props => {
    const store = props.store
    const carrier = store.getCarrierById(props.carrierId)
    const logoUrl = store.getCarrierLogoFullUrl(props.carrierId)

    let classes = 'carrier-logo-default-size'

    if (props.className) {
        classes += ' ' + props.className
    }

    return (
        <img
            className={classes}
            src={logoUrl}
            alt={`Logo przewoźnika ${carrier.name}`}
            title={carrier.name}
            // na środowisku deweloperskim usuwa ostrzeżenia
            crossOrigin="anonymous"
        />
    )
}

export default CarrierLogo
