import React from 'react'
import { withRouter } from 'react-router-dom'
import { goToStationView } from '../helpers/routing'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import './SearchBox.scss'

import StationSelect from './StationSelect'
import { Button } from './Buttons'

class SearchBox extends React.Component {
    constructor(props) {
        super(props)

        this.defaultStationById = this.props.defaultStationById || null
        this.selectedStationId = this.defaultStationById
    }

    handleStationChange = newValue => {
        this.selectedStationId = newValue
    }

    handleSubmit = e => {
        e.preventDefault()

        if (this.selectedStationId === null) {
            return
        }

        const store = this.props.store
        const urlName = store.getStationUrlName(this.selectedStationId)
        goToStationView(this.props.history, urlName)
    }

    render() {
        const stations = this.props.store.stations

        return (
            <div className="search-box">
                <p>Wpisz nazwę stacji, dla której wyświetlić wyniki.</p>
                <form className="input-form" onSubmit={this.handleSubmit}>
                    <StationSelect
                        stations={stations}
                        defaultStationById={this.defaultStationById}
                        onChange={this.handleStationChange}
                    />

                    <Button className="search-btn" icon={faSearch}>
                        Szukaj
                    </Button>
                </form>
            </div>
        )
    }
}

export default withRouter(SearchBox)
