import React from 'react'
import Autosuggest from 'react-autosuggest'
import './StationSelect.scss'

const replacePolishChars = s => {
    s = s.replace(/ę/gi, 'e')
    s = s.replace(/ż/gi, 'z')
    s = s.replace(/ó/gi, 'o')
    s = s.replace(/ł/gi, 'l')
    s = s.replace(/ć/gi, 'c')
    s = s.replace(/ś/gi, 's')
    s = s.replace(/ź/gi, 'z')
    s = s.replace(/ń/gi, 'n')
    s = s.replace(/ą/gi, 'a')
    return s
}

const prepareStationName = name => replacePolishChars(name.toLowerCase())

const stationNameMatchedExactly = (userValue, station) => {
    return prepareStationName(userValue) === prepareStationName(station.name)
}

const getSuggestionValue = suggestion => suggestion.name

const renderSuggestion = suggestion => <div>{suggestion.name}</div>

class StationSelect extends React.Component {
    static defaultProps = {
        defaultStationById: null,
    }

    constructor(props) {
        super(props)

        const stationId = this.props.defaultStationById
        const textValue = this.getDefaultInputValue()

        this.state = {
            stationId, // id bieżącej stacji lub null
            textValue, // tekst aktualnie wpisany przez użytkownika
            suggestions: [], // lista aktualnie wyświetlanych sugesti (obiekty)
        }
    }

    getDefaultInputValue = () => {
        let stationId = this.props.defaultStationById
        // przydatne przy parametrach z URL
        stationId =
            typeof stationId === 'string' ? parseInt(stationId, 10) : stationId

        let textValue = ''

        if (stationId !== null) {
            const station = this.props.stations.find(
                station => station.id === stationId
            )
            textValue = station ? getSuggestionValue(station) : ''
        }

        return textValue
    }

    getSuggestions = value => {
        const inputValue = prepareStationName(value.trim())
        const inputLength = inputValue.length

        if (inputLength < 3) {
            return [] // min 3 znaki wpisane
        } else {
            const result = this.props.stations
                .filter(station =>
                    new RegExp(inputValue).test(
                        prepareStationName(station.name)
                    )
                )
                .sort((a, b) => {
                    /* Sortuj według popularności stacji, chyba że dopasowywany
                     * jest dokładnie początek nazwy - wówczas te preferuj
                     * (np. ław -> Ławica, Wrocław Główny */
                    const aStarts = prepareStationName(a.name).startsWith(
                        inputValue
                    )
                    const bStarts = prepareStationName(b.name).startsWith(
                        inputValue
                    )

                    if (aStarts !== bStarts) {
                        return aStarts ? -1 : 1
                    } else {
                        return b.popularity - a.popularity
                    }
                })
                .splice(0, 5) // pokaż do 5 wyników

            // nie pokazuj podpowiedzi, gdy dokładna fraza wpisana i nie ma innych wyników
            return result.length === 1 &&
                stationNameMatchedExactly(inputValue, result[0])
                ? []
                : result
        }
    }

    onChange = (event, { newValue }) => {
        const station = this.props.stations.find(station =>
            stationNameMatchedExactly(newValue, station)
        )
        const stationId = station === undefined ? null : station.id

        this.setState({
            stationId,
            textValue: newValue,
        })

        if (this.props.onChange && this.state.stationId !== stationId) {
            this.props.onChange(stationId)
        }
    }

    onSuggestionsFetchRequested = ({ value }) => {
        this.setState({
            suggestions: this.getSuggestions(value),
        })
    }

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
        })
    }

    render() {
        const { stationId, textValue, suggestions } = this.state

        // Autosuggest will pass through all these props to the input.
        const inputProps = {
            placeholder: 'np. Kraków Główny',
            value: textValue,
            onChange: this.onChange,
            spellCheck: 'false',
        }

        const invalidValueClass =
            stationId === null && textValue !== '' ? 'invalid-value' : ''

        return (
            <div className={`${this.props.className} ${invalidValueClass}`}>
                <Autosuggest
                    suggestions={suggestions}
                    onSuggestionsFetchRequested={
                        this.onSuggestionsFetchRequested
                    }
                    onSuggestionsClearRequested={
                        this.onSuggestionsClearRequested
                    }
                    getSuggestionValue={getSuggestionValue}
                    renderSuggestion={renderSuggestion}
                    inputProps={inputProps}
                />
            </div>
        )
    }
}

export default StationSelect
