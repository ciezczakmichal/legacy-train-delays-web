import React from 'react'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import './Buttons.scss'

// przy większej wiedzy można uwspólnić z Button...
const ButtonLink = props => {
    const colorClass = props.color || 'primary'
    let className = 'btn-default color-' + colorClass

    if (props.className) {
        className += ` ${props.className}`
    }

    return (
        <Link className={className} to={props.to} onClick={props.onClick}>
            <FontAwesomeIcon icon={props.icon} />
            <span className="label">{props.children}</span>
        </Link>
    )
}

export { ButtonLink }
