import React from 'react'
import './Header.scss'

import CustomLink from './CustomLink'

const Header = () => (
    <header className="header">
        <img
            src={process.env.PUBLIC_URL + '/logo.svg'}
            alt="Logo aplikacji"
            className="logo"
        />
        <h1 className="page-title">
            <CustomLink to="/">Opóźnienia pociągów</CustomLink>
        </h1>
    </header>
)

export default Header
