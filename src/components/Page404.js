import React from 'react'
import { Link } from 'react-router-dom'

const Page404 = () => (
    <div>
        <h2>Nieprawidłowy odnośnik</h2>
        <Link to="/">Przejdź do wyszukiwarki</Link>
    </div>
)

export default Page404
