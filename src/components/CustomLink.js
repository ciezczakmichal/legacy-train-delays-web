import React from 'react'
import { Route, Link as ReactRouterLink } from 'react-router-dom'

// wersja Link, która nie dodaje duplikatów w historii
const CustomLink = ({ to, replace, ...props }) => (
    <Route path={to} exact>
        {({ match }) => (
            <ReactRouterLink {...props} to={to} replace={replace || !!match} />
        )}
    </Route>
)

CustomLink.propTypes = ReactRouterLink.propTypes
CustomLink.defaultProps = ReactRouterLink.defaultProps

export default CustomLink
