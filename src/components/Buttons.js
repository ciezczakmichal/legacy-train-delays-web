import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRedo } from '@fortawesome/free-solid-svg-icons'
import './Buttons.scss'

const Button = props => {
    const colorClass = props.color || 'primary'
    let className = 'btn-default color-' + colorClass

    if (props.className) {
        className += ` ${props.className}`
    }

    return (
        <button className={className} onClick={props.onClick}>
            <FontAwesomeIcon icon={props.icon} />
            <span className="label">{props.children}</span>
        </button>
    )
}

const LineButton = props => {
    let className = 'line-button'

    if (props.className) {
        className += ` ${props.className}`
    }

    return (
        <div className="line-button-container">
            <Button
                className={className}
                icon={props.icon}
                color={props.color}
                onClick={props.onClick}
            >
                {props.children}
            </Button>
        </div>
    )
}

const RefreshButton = props => {
    return (
        <Button
            className={props.className}
            icon={faRedo}
            onClick={props.onClick}
        >
            {props.text || 'Odśwież dane'}
        </Button>
    )
}

export { Button, LineButton, RefreshButton }
