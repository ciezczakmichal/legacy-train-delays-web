import React from 'react'

const Loader = props =>
    props.errorMessage ? (
        <div>
            <h2>{props.errorTitle || 'Krytyczny błąd aplikacji'}</h2>
            <p>{props.errorMessage}</p>
        </div>
    ) : (
        <h3 style={{ textAlign: 'center' }}>
            {props.title || 'Trwa ładowanie aplikacji...'}
        </h3>
    )

export default Loader
