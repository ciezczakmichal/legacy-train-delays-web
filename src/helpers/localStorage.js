/* Klucze stosowane w aplikacji */

const storageKeys = {
    lastSelected: 'lastSelectedStations',
    trackedTrains: 'trackedTrains',
}

/* Funkcje ułatwiające manipulację danymi */

const saveInLocalStorage = (key, value) => {
    const json = JSON.stringify(value)
    localStorage.setItem(key, json)
}

const loadFromLocalStorage = key => {
    const json = localStorage.getItem(key)
    return json === null ? null : JSON.parse(json)
}

export { storageKeys, saveInLocalStorage, loadFromLocalStorage }
