const apiUrl = process.env.REACT_APP_API_URL
const apiUrlVariable = 'REACT_APP_API_URL'

const allow4XXCodes = 'allow'
const disallow4XXCodes = 'disallow'

const fetchData = async (resourceUrl, allow4XX = disallow4XXCodes) => {
    try {
        if (!apiUrl) {
            throw new Error(
                `Nie zdefiniowano adresu API (zmienna środowiskowa ${apiUrlVariable})`
            )
        }

        const url = apiUrl + resourceUrl
        const response = await fetch(url)

        if (
            !response.ok &&
            !(
                allow4XX === allow4XXCodes &&
                response.status >= 400 &&
                response.status < 500
            )
        ) {
            throw new Error(
                'Nieoczekiwana odpowiedź serwera. Kod HTTP ' + response.status
            )
        }

        return response.ok ? response.json() : null
    } catch (error) {
        console.error(
            'Wykonanie zapytania API zakończyło się błędem. ' + error.message
        )
        throw error
    }
}

const assertObject = data => {
    if (!(typeof data === 'object' && data !== null)) {
        throw new Error('Dane powinny mieć postać obiektu')
    }
}

class APIAccess {
    stations = []
    carriers = {}

    async fetchStations() {
        const response = await fetchData('/stations')
        const data = response.stations

        if (!Array.isArray(data)) {
            throw new Error('Dane powinny mieć postać tablicy')
        }

        this.stations = data
    }

    async fetchCarriers() {
        const response = await fetchData('/carriers')
        const data = response.carriers

        assertObject(data)

        this.carriers = data
    }

    async fetchStationTrains(stationId) {
        const data = await fetchData(`/stations/${stationId}/trains`)

        assertObject(data)

        if (data.station.id !== stationId) {
            throw new Error('API zwróciło dane dla nieprawidłowej stacji')
        }

        return data
    }

    // zwraca dane pociągu lub null, jeśli nie udało się pobrać
    async fetchTrain(trainId) {
        const data = await fetchData(`/trains/${trainId}`, allow4XXCodes)

        if (data !== null) {
            assertObject(data)

            if (data.id !== trainId) {
                throw new Error('API zwróciło dane dla nieprawidłowego pociągu')
            }
        }

        return data
    }

    /* Do testów. Zwraca Promise, który wykonuje się po określonym czasie */
    // justAddDelay() {
    //     const delay = 1000

    //     return new Promise((resolve, reject) =>
    //         setTimeout(() => {
    //             resolve()
    //         }, delay)
    //     )
    // }

    /* Pobiera wszystkie statyczne dane wymagane do uruchomienia aplikacji.
     * Zwraca Promise */
    fetchStaticData() {
        return Promise.all([this.fetchStations(), this.fetchCarriers()]).catch(
            error => {
                console.error(error)
                throw new Error(
                    'Pobranie danych wymaganych do uruchomienia aplikacji nie powiodło się'
                )
            }
        )
    }
}

const API = new APIAccess()

export default API
