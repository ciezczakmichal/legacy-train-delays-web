const goToStationView = (history, stationUrlName) =>
    history.push(`/station/${stationUrlName}`)

/* Dodatkowy argument pozwoli komponentowi TrainView ustalić,
 * czy powrót na widok stacji jest możliwy */
const goToTrainView = (history, trainId, callerStationId) =>
    history.push(`/train/${trainId}`, { callerStationId })

export { goToStationView, goToTrainView }
