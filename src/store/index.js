import { storageKeys, saveInLocalStorage } from '../helpers/localStorage'

const lastSelectedMaxCount = 10

const createStore = (
    setStateFunc,
    stations,
    carriers,
    lastSelected,
    trackedTrains
) => {
    return {
        setStateFunc,

        stations,
        carriers,
        lastSelected,
        trackedTrains,

        /* Stacje */

        getStationById(id) {
            return this.stations.find(s => s.id === id) || null
        },

        getStationByUrlName(urlName) {
            return this.stations.find(s => s.urlName === urlName) || null
        },

        getStationName(stationId) {
            const station = this.getStationById(stationId)
            return station ? station.name : ''
        },

        getStationUrlName(stationId) {
            const station = this.getStationById(stationId)
            return station ? station.urlName : ''
        },

        /* Przewoźnicy */

        getCarrierById(id) {
            return this.carriers.data.find(c => c.id === id) || null
        },

        getCarrierLogoFullUrl(carrierId) {
            const carrier = this.getCarrierById(carrierId)

            if (carrier === null) {
                throw new Error('Nierozpoznany przewoźnik o ID ' + carrierId)
            }

            const baseUrl = this.carriers.logoBaseUrl
            return `${baseUrl}${carrier.logoFilename}`
        },

        /* Ostatnio wybrane stacje */

        lastSelectedStationId() {
            return this.lastSelected.length > 0 ? this.lastSelected[0] : null
        },

        addLastSelectedStation(stationId) {
            setStateFunc(prevState => {
                // usuń poprzednie wystąpienie
                const filtered = prevState.lastSelected.filter(
                    id => id !== stationId
                )
                const combined = [stationId, ...filtered]
                const value = combined.slice(0, lastSelectedMaxCount)

                saveInLocalStorage(storageKeys.lastSelected, value)

                return {
                    lastSelected: value,
                }
            })
        },

        deleteLastSelectedStation(stationId) {
            setStateFunc(prevState => {
                const value = prevState.lastSelected.filter(
                    id => id !== stationId
                )

                saveInLocalStorage(storageKeys.lastSelected, value)

                return {
                    lastSelected: value,
                }
            })
        },

        /* Obserwowane pociągi */

        isTrainTracked(trainId) {
            return this.trackedTrains.findIndex(id => id === trainId) > -1
        },

        addTrackedTrain(trainId) {
            if (this.isTrainTracked(trainId)) {
                return
            }

            setStateFunc(prevState => {
                const combined = [...prevState.trackedTrains, trainId]
                saveInLocalStorage(storageKeys.trackedTrains, combined)

                return {
                    trackedTrains: combined,
                }
            })
        },

        deleteTrackedTrain(trainId) {
            setStateFunc(prevState => {
                const value = prevState.trackedTrains.filter(
                    id => id !== trainId
                )

                saveInLocalStorage(storageKeys.trackedTrains, value)

                return {
                    trackedTrains: value,
                }
            })
        },
    }
}

export default createStore
