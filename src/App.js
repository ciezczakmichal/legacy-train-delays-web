import React from 'react'
import { Route, Switch } from 'react-router-dom'
import './App.scss'

import API from './helpers/API'
import { storageKeys, loadFromLocalStorage } from './helpers/localStorage'
import createStore from './store'

import Loader from './components/Loader'
import Header from './components/Header'
import Page404 from './components/Page404'
import StartupView from './views/startup/StartupView'
import StationView from './views/station/StationView'
import TrainView from './views/train/TrainView'

class App extends React.Component {
    state = {
        isLoaded: false,
        errorMessage: null,

        /* dane statyczne API */
        stations: [], // tablica obiektów
        carriers: {}, // obiekt

        /* Tablica ID ostatnio przeglądanych stacji, im dalej w tablicy tym
         * wcześniej stacja przeglądana */
        lastSelected: [],

        /* Tablica ID pociągów, które są obserwowane */
        trackedTrains: [],
    }

    generateStore() {
        const { stations, carriers, lastSelected, trackedTrains } = this.state
        const setStateFunc = (...args) => this.setState(...args)

        return createStore(
            setStateFunc,
            stations,
            carriers,
            lastSelected,
            trackedTrains
        )
    }

    loadLocalStorageData() {
        const lastSelected =
            loadFromLocalStorage(storageKeys.lastSelected) || []
        const trackedTrains =
            loadFromLocalStorage(storageKeys.trackedTrains) || []

        this.setState({
            lastSelected,
            trackedTrains,
        })
    }

    componentDidMount() {
        API.fetchStaticData()
            .then(() =>
                this.setState({
                    isLoaded: true,
                    stations: API.stations,
                    carriers: API.carriers,
                })
            )
            .catch(e =>
                this.setState({
                    errorMessage: e.message,
                })
            )

        this.loadLocalStorageData()
    }

    render() {
        const store = this.generateStore()

        return !this.state.isLoaded ? (
            <Loader errorMessage={this.state.errorMessage} />
        ) : (
            <div className="container">
                <Header />
                <Switch>
                    <Route path="/" exact>
                        <StartupView store={store} />
                    </Route>
                    <Route path="/station/:urlName" exact>
                        <StationView store={store} />
                    </Route>
                    <Route path="/train/:id" exact>
                        <TrainView store={store} />
                    </Route>
                    <Route component={Page404} />
                </Switch>
            </div>
        )
    }
}

export default App
